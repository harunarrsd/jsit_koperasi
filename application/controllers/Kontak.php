<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_kontak();
        $data['pages'] = $this->load->view('pages/v_kontak',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}
}