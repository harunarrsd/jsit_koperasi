<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keranjang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_keranjang();
		$data['sql2'] = $this->m_koperasi->read_keranjang_total();
        $data['pages'] = $this->load->view('pages/v_keranjang',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function edit_keranjang() {
			$id = $this->input->post('id');
			$harga_produk = $this->input->post('harga_produk');
			$data = array(
				'jumlah' => $this->input->post('jumlah'),
				'harga_total' => $harga_produk * $this->input->post('jumlah')
			);
            $this->m_koperasi->update_keranjang($id,$data);
            redirect('keranjang');
	}
	
	function create_pemesanan(){
        $data=$this->m_koperasi->create_pemesanan();
        echo json_encode($data);
	}

	function create_beli(){
		$this->m_koperasi->create_beli();
		$id_keranjang = $_POST['id_keranjang'];
		$jumlah = $_POST['jumlah'];
		$data_documents = array();
		$index = 0;
		$id_beli = $this->db->insert_id();
		foreach($id_keranjang as $datadocuments){
			array_push($data_documents, array(
				'id_keranjang' => $datadocuments,
				'jumlah_beli' => $jumlah[$index],
				'id_beli' => $this->db->insert_id(),
			));
			$index++;
		}
		$this->m_koperasi->create_detail_beli($data_documents);
		redirect('pembayaran/index/'.$id_beli);
	}
	
	public function download_sk(){				
		force_download('upload/sk.pdf',NULL);
	}	
}
