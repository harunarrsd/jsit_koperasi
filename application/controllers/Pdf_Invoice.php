<?php
Class Pdf_Invoice extends CI_Controller{

    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('login');
		}
    }

    function index(){
    }
    
    function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
    
    function ConverToRoman($num){ 
        $n = intval($num); 
        $res = ''; 
    
        //array of roman numbers
        $romanNumber_Array = array( 
            'M'  => 1000, 
            'CM' => 900, 
            'D'  => 500, 
            'CD' => 400, 
            'C'  => 100, 
            'XC' => 90, 
            'L'  => 50, 
            'XL' => 40, 
            'X'  => 10, 
            'IX' => 9, 
            'V'  => 5, 
            'IV' => 4, 
            'I'  => 1); 
    
        foreach ($romanNumber_Array as $roman => $number){ 
            //divide to get  matches
            $matches = intval($n / $number); 
    
            //assign the roman char * $matches
            $res .= str_repeat($roman, $matches); 
    
            //substract from the number
            $n = $n % $number; 
        } 
    
        // return the result
        return $res; 
    } 
    
    function ConverToIndo($num){ 
        $n = floatval($num);
        $res = ''; 
    
        $indoNumber_Array = array( 
            'Januari', 
            'Februari', 
            'Maret', 
            'April', 
            'Mei', 
            'Juni', 
            'Juli', 
            'Agustus', 
            'September', 
            'Oktober', 
            'November', 
            'Desember'); 
        
        $res = $indoNumber_Array[$n-1];
    
        // return the result
        return $res; 
    }
    
    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}

    function invoice(){
        $id=$this->input->post('id_beli');
		$sql = $this->m_koperasi->detail_pesanan($id);
		$sql2 = $this->m_koperasi->read_pembayaran($id);
        $member = $this->m_koperasi->read_member();
        
        foreach($member->result() as $i){
            $member=$i;
        }

        foreach($sql2->result() as $i){
            $sql2=$i;
        }

        $date=date_create($sql2->tanggal_beli);
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, 'cm', PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(2.01,1.76,2.19,0.49);
        $pdf->AddPage('P', 'F4');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $item='';
        $no=0;
        foreach($sql->result() as $items){
            ++$no;
            $item = $item . '
            <tr>
                <th width="2%"></th>
                <th width="5%" align="center">'.$no.'</th>
                <th width="57%">Buku Tematik SD '.$items->nama_kelas.' '.$items->nama_kategori.' <b>'.$items->judul_produk.'</b></th>
                <th width="9%" align="right">'.$items->jumlah.'</th>
                <th width="12%" align="right">'.$this->convert_to_rupiah($items->harga_produk).'</th>
                <th width="13%" align="right">'.$this->convert_to_rupiah($items->harga_total).'</th>
                <th width="2%"></th>
            </tr>
            ';
        }
        $html = '
            <html>
            <body style="font-family:Calibri;font-size:9px;background-image:url('.APPPATH.'/back_inv.png'.');">
                <table border="0px">
                <tr>
                    <th style="width:125px;text-align:center;padding:10px;">
                        <img style="width:110px;" src="'.APPPATH.'../assets/images/logo_ke.png">
                    </th>
                    <th valign="center" style="width:400px;padding-top:10px;">
                        <br><br>
                        <b>KOPERASI BERKAH USAHA TERPADU </b><br> 
                        JSIT  INDONESIA <br>
                        Pondok Nurul Fikri, Jl Tugu Raya Areman Blok R2 
                        Tugu Cimanggis Depok <br>
                        Telp.021 - 22867102 <br>
                        E - mail : koperasijsitindonesia@gmail.com <br>
                    </th>
                </tr>
                <tr>
                    <th width="100%"></th>
                </tr>
                </table>
                <table border="0px">
                    <tr>
                        <th width="2%"></th>
                        <th bgcolor="rgb(193,198,216);" style="border-top:1px double black;border-bottom:1px double black;" align="center" width="96%"><b style="font-size: 17px;">INVOICE</b></th>
                        <th width="2%"></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr width="100%">
                        <th width="2%"></th>
                        <th width="18%" >Invoice</th>
                        <th align="center" width="10%"><b> : </b></th>
                        <th width="52%"><b> '.(000+floatval($sql2->id_beli)).' / '.$this->ConverToRoman(date("m")).' / TEMATIK / '.date("Y").' </b></th>
                    </tr>
                    <tr width="100%">
                        <th width="2%"></th>
                        <th width="18%" >Kepada</th>
                        <th align="center" width="10%"><b> : </b></th>
                        <th width="52%"><b> '.$member->nama_sekolah.' </b></th>
                    </tr>
                    <tr width="100%">
                        <th width="2%"></th>
                        <th width="18%" >Alamat</th>
                        <th align="center" width="10%"><b> : </b></th>
                        <th width="52%"><b> '.$member->alamat_sekolah.' </b></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr width="100%">
                        <th width="2%"></th>
                        <th width="18%" >CP</th>
                        <th align="center" width="10%"><b> : </b></th>
                        <th width="52%"><b> '.$sql2->cp.' </b></th>
                    </tr>
                    <tr width="100%">
                        <th width="2%"></th>
                        <th width="18%" >No. Telepon</th>
                        <th align="center" width="10%"><b> : </b></th>
                        <th width="52%"><b> '.$sql2->no_handphone.' </b></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr align="center" style="border-top:1px double black;border-bottom:1px double black; font-size:10px;font-weight:bold;" bgcolor="rgb(193,198,216)">
                        <th bgcolor="white" width="2%"></th>
                        <th width="5%">No</th>
                        <th width="57%">Item</th>
                        <th width="9%">Qty</th>
                        <th width="12%">Harga</th>
                        <th width="13%">Jumlah</th>
                        <th bgcolor="white" width="2%"></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    '.$item.'
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr align="right" style="font-weight:bold;">
                        <th width="2%"></th>
                        <th width="62%"></th>
                        <th width="21%" align="left">Sub Total</th>
                        <th width="13%">'.$this->convert_to_rupiah($sql2->beli_harga).'</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr align="right" style="font-weight:bold;">
                        <th width="2%"></th>
                        <th width="62%"></th>
                        <th width="21%" align="left">Discount '.((floatval($sql2->diskon)/floatval($sql2->beli_harga))*100).'%</th>
                        <th style="border-bottom:1px double black;" width="13%">'.$this->convert_to_rupiah($sql2->diskon).'</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr align="right" style="font-weight:bold;" >
                        <th width="2%"></th>
                        <th width="62%"></th>
                        <th width="21%" align="left">Total</th>
                        <th width="13%">'.$this->convert_to_rupiah($sql2->total_beli).'</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr>
                        <th width="2%"></th>
                        <th width="96%"><hr></th>
                        <th width="2%"></th>
                    </tr>
                    <tr style="border-top:1px double black;">
                        <th width="2%"></th>
                        <th width="96%">Terbilang : <b>'.ucwords($this->terbilang($sql2->total_beli).' rupiah').'*</br></th>
                        <th width="2%"> </th>
                    </tr>
                    <tr>
                        <th width="100%"></th>
                    </tr>
                    <tr style="font-style:italic;">
                        <th width="2%"></th>
                        <th width="5%" align="center">*</th>
                        <th width="75%">
                        Pembayaran Transfer ke Rekening Bank BNI Syariah <br>
                        No.Rek 8002828252 An Koperasi Berkah Usaha Terpadu
                        </th>
                        <th width="2%"> </th>
                    </tr>
                    <tr>
                        <th width="2%"></th>
                        <th width="5%" align="center"></th>
                        <th width="75%"></th>
                        <th width="2%"> </th>
                    </tr>
                    <tr style="font-style:italic;">
                        <th width="2%"></th>
                        <th width="5%" align="center">*</th>
                        <th width="75%">Bukti transfer harap  di kirim Via WA ke No.0812 800 35249 / Didit</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr>
                        <th height="50px" width="100%"></th>
                    </tr>
                </table>
                <table>
                    <tr align="center">
                        <th width="2%"></th>
                        <th width="44%"></th>
                        <th width="54%">Depok, '.date_format($date,'d ').$this->ConverToIndo(date_format($date,'m')).date_format($date,' Y').'</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr>
                        <th width="44%"></th>
                        <th rowspan="3" align="right" width="54%">
                            <img style="width:270px;" src="'.APPPATH.'../assets/images/ttd_cap.jpg">
                        </th>
                        <th width="2%"></th>
                    </tr>
                    <tr>
                        <th width="44%"></th>
                        <th width="54%"></th>
                        <th width="2%"></th>
                    </tr>
                    <tr>
                        <th width="44%"></th>
                        <th width="54%"></th>
                        <th width="2%"></th>
                    </tr>
                    <tr align="center">
                        <th width="2%"></th>
                        <th width="42%"></th>
                        <th style="border-bottom:1px double black;" width="54%">Adi Wibowo</th>
                        <th width="2%"> </th>
                    </tr>
                    <tr align="center">
                        <th width="2%"></th>
                        <th width="42%"></th>
                        <th width="54%">Koperasi Berkah Usaha Terpadu</th>
                        <th width="2%"> </th>
                    </tr>
                </table>
            </body>    
            </html>';
        $pdf->WriteHTML($html);   
        $pdf->Output();
    }
}