<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
        $data['pages'] = $this->load->view('pages/v_login','',true);
		$this->load->view('master',array('main'=>$data));
	}

	// login
	function ceklogin(){
		if (isset($_POST['login'])) {
			$user=$this->input->post('username',true);
			$pass=md5($this->input->post('password',true));
			$cek=$this->m_koperasi->proseslogin($user, $pass);
			$hasil=count($cek);
			if ($hasil > 0) {
				$yglogin=$this->db->get_where('user',array('username'=>$user, 'password'=>$pass))->row();
				$data = array('udhmasuk' => true,
					'id'=>$yglogin->id_user,
					'id_member'=>$yglogin->id_member,
					'nama' => $yglogin->nama_user
				);
				$this->session->set_userdata($data);
				redirect('.');
			}else {
				$this->session->set_flashdata('notif','<div class="alert alert-danger alert-dismissible"><strong> Maaf Email atau Password Salah ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
				redirect('login');
			}
		}
	}

	// logout
	function logout(){
		$this->session->sess_destroy();
		redirect('.');
	}
}
