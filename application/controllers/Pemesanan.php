<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pemesanan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_pemesanan();
		// $data['sql2'] = $this->m_koperasi->read_detail_beli();
		// $data['sql3'] = $this->m_koperasi->read_pembatalan_pesanan();
        $data['pages'] = $this->load->view('pages/v_pemesanan',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function detail_pesanan($id)
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->detail_pesanan($id);
		// $data['sql3'] = $this->m_koperasi->read_beli();
		$data['sql2'] = $this->m_koperasi->read_pembayaran($id);
		$data['member'] = $this->m_koperasi->read_member();
        $data['pages'] = $this->load->view('pages/v_detail_pesanan',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create_pembayaran(){
		$id = $this->input->post('id_beli');
		$cp = $this->input->post('cp');
		$no_handphone = $this->input->post('no_handphone');
		$data = array(
			'cp' => $cp,
			'no_handphone' => $no_handphone
		);
		$this->m_koperasi->update_beli($id,$data);

		$filename = date("YmdHis").'_'.$id;
		$config = array(
			'upload_path'=>'upload/bukti_bayar/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename
		);
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto_bukti')){
			$finfo = $this->upload->data();
			$gambar = $finfo['file_name'];

			$data_pembayaran = array(
				'status_bayar' => 'Sudah',
				'foto_bukti_pembayaran' => $gambar
			);

			$kode_id = array('id_pembayaran'=>$id);
			$gambar_db = $this->db->get_where('pembayaran',$kode_id);
			if($gambar_db->num_rows()>0){
				$pros=$gambar_db->row();
				$name_gambar=$pros->foto_bukti_pembayaran;

				if(file_exists($lok=FCPATH.'upload/bukti_bayar/'.$name_gambar)){
				  unlink($lok);
				}
			}
			$this->m_koperasi->update_pembayaran($id,$data_pembayaran);
		}
		redirect('pemesanan/detail_pesanan/'.$id);
	}

	function batalkan_pesanan(){
		$id = $this->input->post('id_beli');
		$data = array(
			'status' => 'Batal',
		);
		$this->m_koperasi->update_beli($id,$data);
		redirect('pemesanan');
	}

	function hapus_foto_bayar($id){
		$this->m_koperasi->delete_foto_bayar($id);
		// $this->db->where("id_beli", $id);
        // $query = $this->db->get('pembayaran');
        // $row = $query->row();
        // unlink("upload/bukti_bayar/$row->foto_bukti_pembayaran");
		$data = array(
			'status_bayar' => 'Belum',
			'foto_bukti_pembayaran' => NULL
		);
		$this->m_koperasi->update_pembayaran($id,$data);
		redirect('pemesanan/detail_pesanan/'.$id);
	}

	function foto_bukti(){
		$id = $this->input->post('id_beli');
		$filename = date("YmdHis").'_'.$id;
		$config = array(
			'upload_path'=>'upload/bukti_terima/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename
		);
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto_bukti_terima')){
			$finfo = $this->upload->data();
			$gambar = $finfo['file_name'];

			$data = array(
				'status' => "Terima",
				'foto_bukti_terima' => $gambar
			);

			$kode_id = array('id_beli'=>$id);
			$gambar_db = $this->db->get_where('beli',$kode_id);
			if($gambar_db->num_rows()>0){
				$pros=$gambar_db->row();
				$name_gambar=$pros->foto_bukti_terima;

				if(file_exists($lok=FCPATH.'upload/bukti_terima/'.$name_gambar)){
				  unlink($lok);
				}
			}
			$this->m_koperasi->update_beli($id,$data);
		}
		redirect('pemesanan');
	}

	function pesan_komplain(){
		$id = $this->input->post('id_beli');
		$pesan = $this->input->post('pesan');
		$data = array(
			'id_beli' => $id,
			'pesan' => $pesan,
			'tanggal_komplain' => date('Y-m-d H:i:s')
		);
		$this->m_koperasi->create_komplain($data);
		redirect('pemesanan');
	}

	// function update_pemesanan(){
	// 	$data=$this->m_koperasi->update_pemesanan();
	// 	echo json_encode($data);
	// }

	public function invoice($id)
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->detail_pesanan($id);
		$data['sql2'] = $this->m_koperasi->read_pembayaran($id);
		$data['member'] = $this->m_koperasi->read_member();
        $data['pages'] = $this->load->view('pages/v_invoice',array('main'=>$data),true);
		$this->load->view('master_invoice',array('main'=>$data));
	}
}
