<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_beranda();
		$data['etalase'] = $this->m_koperasi->read_etalase_produk();
        $data['pages'] = $this->load->view('pages/v_main',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function data_notifikasi(){
		$data=$this->m_koperasi->read_notifikasi()->result();
		echo json_encode($data);
	}

	function data_notifikasi_baca(){
		$data=$this->m_koperasi->read_notifikasi_baca()->result();
		echo json_encode($data);
	}

	function update_notif(){
		$data=$this->m_koperasi->update_notif();
		echo json_encode($data);
	}
}
