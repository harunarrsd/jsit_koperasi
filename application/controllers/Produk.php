<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['produk'] = $this->m_koperasi->read_produk();
        $data['pages'] = $this->load->view('pages/v_produk',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function data_produk(){
		$data=$this->m_koperasi->read_produk()->result();
		echo json_encode($data);
	}

	function data_keranjang(){
		$data=$this->m_koperasi->read_keranjang()->result();
		echo json_encode($data);
	}

	function data_keranjang_total(){
		$data=$this->m_koperasi->read_keranjang_total()->result();
		echo json_encode($data);
	}

	function create(){
        $data=$this->m_koperasi->create_keranjang();
        echo json_encode($data);
	}
	
	function kosong_keranjang(){
        $data=$this->m_koperasi->kosong_keranjang();
        echo json_encode($data);
    }
}
