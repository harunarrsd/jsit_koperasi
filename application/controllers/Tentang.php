<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_tentang();
        $data['pages'] = $this->load->view('pages/v_tentang',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}
}
