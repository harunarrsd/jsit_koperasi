<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
        $data['pages'] = $this->load->view('pages/v_register','',true);
		$this->load->view('master',array('main'=>$data));
	}

	function create_member(){
		$this->m_koperasi->create_member();
		$data = array(
			'nama_user'  => $this->input->post('nama_sekolah'),
			'username'  => $this->input->post('id_registrasi'),
			'password' => MD5($this->input->post('password')),
			'id_member' => $this->db->insert_id(),
			'role' => '2'
		);
		$this->m_koperasi->create_user($data);
		redirect('login');
	}

	function get_member(){
		$id_registrasi=$this->input->post('id_registrasi');
		$data=$this->m_koperasi->get_member($id_registrasi);
		echo json_encode($data);
	}

	function provinsi()
	{
		$query = [];
		
		$provinsi = $this->input->get('provinsi');

		if(!empty($provinsi)){
			$query = $this->m_koperasi->get_provinsi($provinsi, 'nama_provinsi');
		}
		echo json_encode($query);
	}

	function kabkot()
	{
		$query = [];
		
		$kabkot = $this->input->get('kabkot');

		if(!empty($kabkot)){
			$query = $this->m_koperasi->get_kabkot($kabkot, 'nama');
		}
		echo json_encode($query);
	}
}
