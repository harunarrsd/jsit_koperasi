<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_koperasi->read_member();
        $data['pages'] = $this->load->view('pages/v_profile',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function update_member(){
		$id = $this->input->post('id_member');
		$data = array(
			'id_registrasi'  => $this->input->post('id_registrasi'),
			'nama_sekolah'  => $this->input->post('nama_sekolah'),
			'jenjang_sekolah' => $this->input->post('jenjang_sekolah'),
			'alamat_sekolah' => $this->input->post('alamat_sekolah'),
			'id_js' => $this->input->post('id_js'),
			'email_sekolah' => $this->input->post('email_sekolah'),
			'provinsi_sekolah' => $this->input->post('provinsi_sekolah'),
			'id_provinsi' => $this->input->post('id_provinsi'),
			'kabupaten_kota_sekolah' => $this->input->post('kabupaten_kota_sekolah'),
			'id_kabkot' => $this->input->post('id_kabkot'),
			'kordinat_geografis' => $this->input->post('id_kabkot'),
			'kode_regional' => $this->input->post('kode_regional'),
		);
		$this->m_koperasi->update_member($id,$data);
		redirect('profile');
	}
}
