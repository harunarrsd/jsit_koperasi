<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('login');
		}
	}

	public function index($id)
	{
		$data['title'] = 'Koperasi JSIT';
		// $data['sql'] = $this->m_koperasi->read_beli();
		// $id = $this->db->insert_id();
		$data['sql2'] = $this->m_koperasi->read_pembayaran($id);
		$data['member'] = $this->m_koperasi->read_member();
        $data['pages'] = $this->load->view('pages/v_pembayaran',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	// function update_pemesanan(){
	// 	$data=$this->m_koperasi->update_pemesanan();
	// 	echo json_encode($data);
	// }

	function create_pembayaran(){
		$id = $this->input->post('id_beli');
		$cp = $this->input->post('cp');
		$no_handphone = $this->input->post('no_handphone');
		$data = array(
			'cp' => $cp,
			'no_handphone' => $no_handphone
		);
		$this->m_koperasi->update_beli($id,$data);

		$filename = date("YmdHis").'_'.$id;
		$config = array(
			'upload_path'=>'upload/bukti_bayar/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename
		);
		$this->upload->initialize($config);
		if($this->upload->do_upload('foto_bukti')){
			$finfo = $this->upload->data();
			$gambar = $finfo['file_name'];

			$data_pembayaran = array(
				'status_bayar' => 'Sudah',
				'foto_bukti_pembayaran' => $gambar
			);

			$kode_id = array('id_pembayaran'=>$id);
			$gambar_db = $this->db->get_where('pembayaran',$kode_id);
			if($gambar_db->num_rows()>0){
				$pros=$gambar_db->row();
				$name_gambar=$pros->foto_bukti_pembayaran;

				if(file_exists($lok=FCPATH.'upload/bukti_bayar/'.$name_gambar)){
				  unlink($lok);
				}
			}
			$this->m_koperasi->update_pembayaran($id,$data_pembayaran);
		}
		redirect('.');
	}
}
