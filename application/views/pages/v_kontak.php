<!-- Title Page -->
<?php
	foreach($main['sql']->result() as $sql){
		$judul_halaman = $sql->judul_halaman;
		$cover_halaman = $sql->cover_halaman;
		$link_maps = $sql->link_maps;
	}
?>
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(http://localhost/jsit_koperasi_admin/upload/config_kontak/<?php echo $cover_halaman;?>);">
	<h2 class="l-text2 t-center">
		<?php echo $judul_halaman;?>
	</h2>
</section>

<!-- content page -->
<section class="bgwhite p-t-66 p-b-60">
	<div class="container">
		<div class="row">
			<div class="col-md-6 p-b-30">
				<div class="p-r-20 p-r-0-lg">
					<iframe src="<?php echo $link_maps;?>" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					<!-- <div class="contact-map size21" id="google_map" data-map-x="40.614439" data-map-y="-73.926781" data-pin="<?php echo base_url();?>assets/images/icons/icon-position-map.png" data-scrollwhell="0" data-draggable="1"></div> -->
				</div>
			</div>

			<div class="col-md-6 p-b-30">
				<form class="leave-comment">
					<h4 class="m-text26 p-b-36 p-t-15">
						Kirim Pesan
					</h4>

					<div class="bo4 of-hidden size15 m-b-20">
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="name" placeholder="Nama Anda">
					</div>

					<div class="bo4 of-hidden size15 m-b-20">
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email" placeholder="Alamat email Anda">
					</div>

					<textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="message" placeholder="Pesan yang ingin Anda sampaikan"></textarea>

					<div class="w-size25">
						<!-- Button -->
						<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
							Kirim
						</button>
					</div>
				</form>
				<br>
				<div class="text-right">
					<h6>Contact Person :</h6>
					<ul>
						<li>Anis Khaerunnisa - 0813-8110-8877</li>
						<li>Nadya - 0857-7192-8944</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
