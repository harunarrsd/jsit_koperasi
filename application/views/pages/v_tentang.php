<!-- Title Page -->
<?php
	foreach($main['sql']->result() as $sql){
		$judul_halaman = $sql->judul_halaman;
		$cover_halaman = $sql->cover_halaman;
		$foto_perusahaan = $sql->foto_perusahaan;
		$judul_tentang = $sql->judul_tentang;
		$isi_tentang = $sql->isi_tentang;
		$nama_quotes = $sql->nama_quotes;
		$isi_quotes = $sql->isi_quotes;
	}
?>
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(http://localhost/jsit_koperasi_admin/upload/config_tentang/<?php echo $cover_halaman;?>);">
	<h2 class="l-text2 t-center">
		<?php echo $judul_halaman;?>
	</h2>
</section>

<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
	<div class="container">
		<div class="row">
			<div class="col-md-4 p-b-30">
				<div class="hov-img-zoom">
					<img src="http://localhost/jsit_koperasi_admin/upload/config_tentang/<?php echo $foto_perusahaan;?>" alt="IMG-ABOUT">
				</div>
			</div>

			<div class="col-md-8 p-b-30">
				<h3 class="m-text26 p-t-15 p-b-16">
					<?php echo $judul_tentang;?>
				</h3>

				<p class="p-b-28">
					<?php echo $isi_tentang;?>
				</p>

				<br>

				<div class="bo13 p-l-29 m-l-9 p-b-10">
					<p class="p-b-11">
						<?php echo $isi_quotes;?>
					</p>

					<span class="s-text7">
						- <?php echo $nama_quotes;?>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>
