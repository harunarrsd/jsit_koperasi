<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 p-b-30">
			<?php echo form_open('login/ceklogin')?>
			<form>
				<h3 class="m-text26 p-t-15 p-b-16">
					Halaman Login
				</h3>
				<?php echo $this->session->flashdata('notif')?>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="username" placeholder="ID Registrasi">
				</div>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" placeholder="Password">
				</div>
				<div class="m-b-20">
					<p align="center">Belum punya akun? <a href="<?php echo base_url('register');?>">Buat akun.</a></p>
				</div>
				<div class="w-size25 align-center">
					<button type="submit" name="login" class="size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
						Login
					</button>
				</div>
			</form>
			<?php echo form_close()?>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</section>
