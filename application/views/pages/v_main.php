<!-- Slide1 -->
<section class="slide1">
	<div class="wrap-slick1">
		<div class="slick1">
			<?php
			foreach ($main['sql']->result() as $sql) {
				?>
				<div class="item-slick1 item1-slick1" style="background-image: url(http://localhost/jsit_koperasi_admin/upload/config_beranda/<?php echo $sql->background; ?>);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15" data-appear="fadeInDown">
							<?php echo $sql->caption_satu; ?>
						</span>

						<h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="fadeInUp">
							<?php echo $sql->caption_dua; ?>
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
							<a href="<?php echo site_url('produk');?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								<?php echo $sql->text_button; ?>
							</a>
						</div>
					</div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</section>

<!-- New Product -->
<section class="newproduct bgwhite p-t-45">
	<div class="container">
		<div class="sec-title p-b-60">
			<h3 class="m-text5 t-center">
				Etalase Produk
			</h3>
		</div>

		<!-- Slide2 -->
		<div class="wrap-slick2">
			<div class="slick2">
				<?php
				foreach ($main['etalase']->result() as $data) {
					?>
					<div class="item-slick2 p-l-15 p-r-15">
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img src="http://localhost/jsit_koperasi_admin/upload/produk/<?php echo $data->foto_produk;?>" alt="IMG-PRODUCT">
								<div class="block2-overlay trans-0-4">
									<div class="block2-btn-addcart w-size1 trans-0-4">
										<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4 <?php if ($this->session->userdata('udhmasuk')==true) { echo "item_edit_main";} else { echo "login_dulu_main"; }?>" data-id_produk="<?php echo $data->id_produk;?>" data-harga_produk="<?php echo $data->harga_produk;?>" data-judul_produk="<?php echo $data->judul_produk;?>" data-stok="<?php echo $data->stok;?>">
											Tambah ke Keranjang
										</button>
									</div>
								</div>
							</div>

							<div class="block2-txt p-t-20">
								<p><?php echo $data->nama_kelas;?></p>
								<a href="<?php echo base_url('.'); ?>" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $data->nama_kategori;?> <?php echo $data->judul_produk;?>
								</a>
								<span class="block2-price m-text6 p-r-5">
									Rp <?php echo $data->harga_produk;?>
								</span>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="keranjangg_main" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Tambah ke keranjang</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<div class="bo4 of-hidden size15 m-b-20">
				<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jumlah" id="jumlah" placeholder="Jumlah" required>
			</div>
		</div>
		<input type="hidden" name="id_produk" id="id_produk"">
		<input type="hidden" name="harga_produk" id="harga_produk"">
		<input type="hidden" name="stok" id="stok"">
	  </div>
	  <div class="modal-footer">
		<button type="button" type="submit" id="btn_save" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Tambah
		</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="login_dulu_main" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Information</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<p>Jika ingin menambahkan ke keranjang, harap melakukan login terlebih dahulu.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Tidak</button>
		<a href="<?php echo site_url('login')?>"  class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4 text-center">
			Iya
		</a>
	  </div>
	</div>
  </div>
</div>
