<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<section class="cart bgwhite p-t-70 p-b-100">
	<div class="container">
		<h1 class="text-center">Pembayaran</h1>
		<br><br>
		<div class="bo9 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
		<?php echo form_open_multipart('pembayaran/create_pembayaran/');?>
			<div class="row">
				<div class="col-md-8">
					<h5 class="m-text20 p-b-24">
						Form Pembayaran
					</h5>

					<?php
					foreach($main['member']->result() as $sql){
					?>
					<div class="row">
						<div class="col-md-6">
							<label for="id_registrasi">ID Registrasi</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="number" name="id_registrasi" id="id_registrasi" placeholder="ID Registrasi" value="<?php echo $sql->id_registrasi;?>" readonly>
							</div>
							<label for="nama_sekolah">Nama Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="nama_sekolah" id="nama_sekolah" placeholder="Nama Sekolah" value="<?php echo $sql->nama_sekolah;?>" readonly>
							</div>
							<label for="jenjang_sekolah">Jenjang Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jenjang_sekolah" id="jenjang_sekolah" placeholder="Jenjang Sekolah" value="<?php echo $sql->jenjang_sekolah;?>" readonly>
							</div>
							<label for="email_sekolah">Email Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email_sekolah" id="email_sekolah" placeholder="Email Sekolah" value="<?php echo $sql->email_sekolah;?>" readonly>
							</div>
						</div>
						<div class="col-md-6">
							<label for="kode_regional">Regional</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kode_regional" id="kode_regional" placeholder="Regional" value="<?php echo $sql->kode_regional;?>" readonly>
							</div>
							<label for="alamat_sekolah">Alamat Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah" value="<?php echo $sql->alamat_sekolah;?>" readonly>
								<!-- <textarea class="sizefull s-text7 p-l-22 p-r-22 p-t-20" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah"></!--> -->
							</div>
							<label for="provinsi_sekolah">Provinsi</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="provinsi_sekolah" id="provinsi_sekolah" placeholder="Provinsi" value="<?php echo $sql->provinsi_sekolah;?>" readonly>
							</div>
							<label for="kabupaten_kota_sekolah">Kabupaten / Kota</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kabupaten_kota_sekolah" id="kabupaten_kota_sekolah" placeholder="Kabupaten / Kota Sekolah" value="<?php echo $sql->kabupaten_kota_sekolah;?>" readonly>
							</div>
						</div>
					</div>
					<?php
					}
					?>

					<?php 
					foreach($main['sql2']->result() as $sql){
					?>
					<div class="row">
						<div class="col-md-6">
							<label for="cp">Nama Contact Person</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="cp" id="cp" placeholder="Nama Contact Person" value="<?php echo $sql->cp;?>">
							</div>
						</div>
						<div class="col-md-6">
							<label for="no_handphone">Nomor Handhphone</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="no_handphone" id="no_handphone" placeholder="Nomor Handphone Contact Person" value="<?php echo $sql->no_handphone;?>">
							</div>
						</div>
					</div>
					<?php
					}?>
					<?php 
					foreach($main['sql2']->result() as $sql){
						if($sql->status_bayar=='Sudah'){
					?>
					<label>Foto Bukti Pembayaran</label>
					<div class="bo4 of-hidden size15 m-b-20">
						<p class="p-l-22 p-r-22 p-t-10"><span class="lnr lnr-checkmark-circle"></span> Sudah Bayar</p>
					</div>
					<?php
						}else{
					?>
					<label>Foto Bukti Pembayaran</label>
					<div class="bo4 of-hidden size15 m-b-20">
						<input class="sizefull s-text7 p-l-22 p-r-22 p-t-10" type="file" name="foto_bukti">
					</div>
					<?php
						}
					}?>
					<p>*Note: Jika belum melakukan pembayaran, harap isi Contact Person dan Nomor Handhphone terlebih dahulu. Lalu, klik tombol Simpan Pembayaran. <br> <a href="<?php echo site_url('.')?>">Klik disini untuk kembali ke halaman utama.</a></p>
				</div>
				<div class="col-md-4">
					<?php 
					foreach($main['sql2']->result() as $sql){
					?>
					<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
					<h5 class="m-text20 p-b-24">
						Keranjang Total
					</h5>

					<div class="flex-w flex-sb-m p-b-12">
						<span class="s-text18 w-size19 w-full-sm">
							Subtotal:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->beli_harga);?>
						</span>
					</div>

					<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
						<span class="s-text18 w-size19 w-full-sm">
							Jumlah:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo $sql->jumlah_total;?>
						</span>

						<span class="s-text18 w-size19 w-full-sm">
							Bonus:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo $sql->bonus;?>
						</span>

						<span class="s-text18 w-size19 w-full-sm">
							Diskon:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->diskon);?>
						</span>
					</div>

					<div class="flex-w flex-sb-m p-t-26 p-b-30">
						<span class="m-text22 w-size19 w-full-sm">
							Total:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->total_beli);?>
						</span>
					</div>
					<?php
					}
					?>
					<div class="size15 trans-0-4">
						<button type="submit" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Simpan Pembayaran
						</button>
					</div>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</section>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
