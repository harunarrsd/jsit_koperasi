<!-- Content page -->
<section class="bgwhite p-t-55 p-b-65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 p-b-50">
				<div class="flex-sb-m flex-w p-b-35">
					<div class="flex-w">
						
					</div>

					<span class="s-text8 p-t-5 p-b-5">
						Showing 1–<?php echo $main['produk']->num_rows();?> of <?php echo $main['produk']->num_rows();?> results
					</span>
				</div>

				<!-- Product -->
				<div class="row" id="show_data">
					
				</div>

				<!-- Pagination -->
				<div class="pagination flex-m flex-w p-t-26">
					<a href="<?php echo site_url('.');?>" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
					<a href="<?php echo site_url('.');?>" class="item-pagination flex-c-m trans-0-4">2</a>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="keranjangg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Tambah ke keranjang</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<div class="bo4 of-hidden size15 m-b-20">
				<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jumlah" id="jumlah" placeholder="Jumlah" required>
			</div>
		</div>
		<input type="hidden" name="id_produk" id="id_produk">
		<input type="hidden" name="harga_produk" id="harga_produk">
		<input type="hidden" name="stok" id="stok">
	  </div>
	  <div class="modal-footer">
		<button type="button" type="submit" id="btn_save" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Tambah
		</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="login_dulu" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Information</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<p>Jika ingin menambahkan ke keranjang, harap melakukan login terlebih dahulu.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Tidak</button>
		<a href="<?php echo site_url('login')?>"  class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4 text-center">
			Iya
		</a>
	  </div>
	</div>
  </div>
</div>

