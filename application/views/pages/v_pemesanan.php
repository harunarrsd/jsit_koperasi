<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<section class="cart bgwhite p-t-70 p-b-100">
	<div class="container">
		<h1 class="text-center">Pesanan</h1>
		<br><br>
		<div class="container-table-cart pos-relative">
			<div class="wrap-table-shopping-cart bgwhite">
				<table class="table-shopping-cart">
					<thead>
						<tr class="table-head">
							<th class="column-1">No</th>
							<th class="column-5 text-center">Produk</th>
							<th class="column-5 text-center">Jumlah</th>
							<th class="column-5 text-center">Total</th>
							<th class="column-5 text-center">Status Pesanan</th>
							<th class="column-5 text-center">Pilihan</th>
						</tr>
					</thead>

					<tbody>
						<?php
							$data = $main['sql']->result();
							// $data2 = $main['sql2']->result();
							$cek = $main['sql']->num_rows();
							if($cek==0){
						?>
						<tr>
							<td>
								<h6 align="center">Pesanan Masih Kosong :(</h6>
							</td>
						</tr>
						<?php
							}else{
								$no = 0;
								foreach($data as $sql){
									$no++;
						?>
						<tr class="table-row">
							<td class="column-1">
								<?php echo $no;?>
							</td>
							<td class="column-5 text-center">
							<?php
							$data2 = $this->db->query('SELECT * FROM detail_beli JOIN beli using(id_beli) join keranjang using(id_keranjang) JOIN produk using(id_produk) where keranjang.id_user = "'.$this->session->userdata('id').'" AND keranjang.beli = 1 AND detail_beli.id_beli = "'.$sql->id_beli.'" ');
							foreach($data2->result() as $sql2){
							?>
							<?php echo $sql2->judul_produk;?>,
							<?php
							} 
							?>
							</td>
							<td class="column-5 text-center"><?php echo $sql->jumlah_total;?></td>
							<td class="column-5 text-center"><?php echo convert_to_rupiah($sql->total_beli);?></td>
							<td class="column-5 text-center">Pesanan <?php echo $sql->status;?></td>
							<td class="column-5">
								<a href="<?php echo site_url('pemesanan/detail_pesanan/');?><?php echo $sql->id_beli;?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">
									Detail Pesanan
								</a><br>
								<?php
								$data3 = $this->db->query('SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = "' . $this->session->userdata('id') . '" AND tgl_pembatalan IS NOT NULL AND notifikasi.id_beli = "'.$sql->id_beli.'" GROUP BY id_notifikasi');
								foreach($data3->result() as $sql3){
									if($sql3->tgl_pembatalan>date("Y-m-d H:i:s")){
								?>
								<?php echo form_open_multipart('pemesanan/batalkan_pesanan/');?>
								<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
									<button type="submit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">
										Batalkan Pesanan
									</button>
								<?php echo form_close()?>
								<?php
									}
									if($sql3->status == "Kirim"){
								?>
									<p class="text-center p-b-10">Pesanan Sudah Sampai?</p>
									<a href="#" data-toggle="modal" data-target=".konfirmasi<?php echo $sql->id_beli;?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">Konfirmasi Pesanan</a>
									<div class="modal fade konfirmasi<?php echo $sql->id_beli;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
									<?php echo form_open_multipart('pemesanan/foto_bukti/');?>
									<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
										<div class="modal-dialog modal-md" role="document">
											<div class="modal-content">
											<div class="modal-header">
												<h6>Konfirmasi Pesanan</h6>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">
												<h6>Upload foto sebagai bukti bahwa pesanan sudah di terima</h6><br>
												<div class="bo4 of-hidden size15 m-b-20">
													<input class="sizefull s-text7 p-l-22 p-r-22 p-t-10" type="file" name="foto_bukti_terima" required>
												</div>
											</div>
											<div class="modal-footer">
											<button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Kembali</button>
												<button type="submit" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
													Submit
												</button>
											</div>
											</div>
										</div>
									<?php echo form_close();?>
									</div>
								<?php
									}
								}
								$data4 = $this->db->query('SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = "' . $this->session->userdata('id') . '" AND tgl_komplain IS NOT NULL AND notifikasi.id_beli = "'.$sql->id_beli.'" GROUP BY id_notifikasi');
								foreach($data4->result() as $sql4){
									if($sql4->tgl_komplain>date("Y-m-d H:i:s")){
								?>
									<a href="#" data-toggle="modal" data-target=".komplain<?php echo $sql->id_beli;?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">Komplain</a>
									<div class="modal fade komplain<?php echo $sql->id_beli;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
									<?php echo form_open_multipart('pemesanan/pesan_komplain/');?>
									<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
										<div class="modal-dialog modal-md" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h6>Komplain Pesanan</h6>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												</div>
												<div class="modal-body">
													<h6>Tinggalkan pesan...</h6><br>
													<textarea name="pesan" id="pesan" placeholder="Pesan" class="sizefull s-text7 p-l-22 p-r-22 p-t-10"></textarea>
												</div>
												<div class="modal-footer">
												<button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Kembali</button>
													<button type="submit" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
														Submit
													</button>
												</div>
											</div>
										</div>
									<?php echo form_close();?>
									</div>
								<?php
									}
								}
								?>
							</td>
						</tr>
						<?php
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal_pesanan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Pesanan</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		  <h6>Apakah Pesanan sudah termasuk dengan guru yang mengajar ?</h6>
		<input type="hidden" name="id_keranjang" id="id_keranjang"">
		<input type="hidden" name="harga_total" id="harga_total">
		<input type="hidden" name="jumlah" id="jumlah">
	  </div>
	  <div class="modal-footer">
	  <button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Belum</button>
		<button type="button" type="submit" id="btn_pesan" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Sudah
		</button>
	  </div>
	</div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.item_pesan').on('click',function(){
		var id_keranjang = $(this).data('id_keranjang');
		var harga_total = $(this).data('harga_total');
		var jumlah = $(this).data('jumlah');
		
		$('#modal_pesanan').modal('show');
		$('[name="id_keranjang"]').val(id_keranjang);
		$('[name="harga_total"]').val(harga_total);
		$('[name="jumlah"]').val(jumlah);
	});

	$('#btn_pesan').on('click',function(){
		var id_keranjang = $('#id_keranjang').val();
		var jumlah = $('#jumlah').val();
		var harga_total = $('#harga_total').val();
		$.ajax({
				type : "POST",
				url  : "<?php echo site_url('keranjang/create_pemesanan')?>",
				dataType : "JSON",
				data : {id_keranjang:id_keranjang, jumlah:jumlah , harga_total:harga_total},
				success: function(data){
					$('[name="id_keranjang"]').val("");
					$('[name="jumlah"]').val("");
					$('[name="harga_total"]').val("");
					$('#modal_pesanan').modal('hide');
					swal("", "Pesanan berhasil dibuat !", "success");
					window.location.href = "<?php echo site_url('pembayaran');?>";
				}
		});
		return false;
	});
});
</script>
