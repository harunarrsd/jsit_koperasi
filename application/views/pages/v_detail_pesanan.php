<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<section class="cart bgwhite p-t-70 p-b-100">
	<div class="container">
		<h1 class="text-center">Detail Pesanan</h1>
		<br><br>
		<a href="<?php echo site_url('pemesanan');?>" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4 p-l-10 p-t-10 p-b-10 p-r-10">
			<span class="lnr lnr-chevron-left"></span>
			Kembali
		</a><br><br>
		<div class="container-table-cart pos-relative">
			<div class="wrap-table-shopping-cart bgwhite">
				<table class="table-shopping-cart">
					<thead>
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">Produk</th>
							<th class="column-3">Harga</th>
							<th class="column-4">Kuantitas</th>
							<th class="column-5">Total</th>
						</tr>
					</thead>

					<tbody>
						<?php
							$data = $main['sql']->result();
							$cek = $main['sql']->num_rows();
							foreach($data as $sql){
						?>
						<tr class="table-row">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="http://localhost/jsit_koperasi_admin/upload/produk/<?php echo $sql->foto_produk;?>" alt="IMG-PRODUCT">
								</div>
							</td>
							<td class="column-2"><?php echo $sql->nama_kategori;?> <?php echo $sql->judul_produk;?></td>
							<td class="column-3"><?php echo convert_to_rupiah($sql->harga_produk);?></td>
							<td class="column-4">
								<?php echo $sql->jumlah;?>
							</td>
							<td class="column-5"><?php echo convert_to_rupiah($sql->harga_total);?></td>
						</tr>
						<?php
							}
						?>
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="bo9 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
		<?php echo form_open_multipart('pemesanan/create_pembayaran/');?>
			<div class="row">
				<div class="col-md-8">
					<h5 class="m-text20 p-b-24">
						Form Pembayaran
					</h5>
					<?php
					foreach($main['member']->result() as $sql){
					?>
					<div class="row">
						<div class="col-md-6">
							<label for="id_registrasi">ID Registrasi</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="number" name="id_registrasi" id="id_registrasi" placeholder="ID Registrasi" value="<?php echo $sql->id_registrasi;?>" readonly>
							</div>
							<label for="nama_sekolah">Nama Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="nama_sekolah" id="nama_sekolah" placeholder="Nama Sekolah" value="<?php echo $sql->nama_sekolah;?>" readonly>
							</div>
							<label for="jenjang_sekolah">Jenjang Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jenjang_sekolah" id="jenjang_sekolah" placeholder="Jenjang Sekolah" value="<?php echo $sql->jenjang_sekolah;?>" readonly>
							</div>
							<label for="email_sekolah">Email Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email_sekolah" id="email_sekolah" placeholder="Email Sekolah" value="<?php echo $sql->email_sekolah;?>" readonly>
							</div>
						</div>
						<div class="col-md-6">
							<label for="kode_regional">Regional</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kode_regional" id="kode_regional" placeholder="Regional" value="<?php echo $sql->kode_regional;?>" readonly>
							</div>
							<label for="alamat_sekolah">Alamat Sekolah</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah" value="<?php echo $sql->alamat_sekolah;?>" readonly>
								<!-- <textarea class="sizefull s-text7 p-l-22 p-r-22 p-t-20" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah"></!-->
							</div>
							<label for="provinsi_sekolah">Provinsi</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="provinsi_sekolah" id="provinsi_sekolah" placeholder="Provinsi" value="<?php echo $sql->provinsi_sekolah;?>" readonly>
							</div>
							<label for="kabupaten_kota_sekolah">Kabupaten / Kota</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kabupaten_kota_sekolah" id="kabupaten_kota_sekolah" placeholder="Kabupaten / Kota Sekolah" value="<?php echo $sql->kabupaten_kota_sekolah;?>" readonly>
							</div>
						</div>
					</div>
					<?php
					}
					?>

					<?php 
					foreach($main['sql2']->result() as $sql){
					?>
					<div class="row">
						<div class="col-md-6">
							<label for="cp">Nama Contact Person</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="cp" id="cp" placeholder="Nama Contact Person" value="<?php echo $sql->cp;?>">
							</div>
						</div>
						<div class="col-md-6">
							<label for="no_handphone">Nomor Handhphone</label>
							<div class="bo4 of-hidden size15 m-b-20">
								<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="no_handphone" id="no_handphone" placeholder="Nomor Handphone Contact Person" value="<?php echo $sql->no_handphone;?>">
							</div>
						</div>
					</div>
					<?php
					}?>
					<?php 
					foreach($main['sql2']->result() as $sql){
						if($sql->status_bayar=='Sudah'){
					?>
					<label>Foto Bukti Pembayaran</label>
					<div class="bo4 of-hidden size15 m-b-20">
						<p class="p-l-22 p-r-22 p-t-10"><span class="lnr lnr-checkmark-circle"></span> Sudah Bayar</p>
					</div><br>
					<div class="size15 trans-0-4">
						<a href="<?php echo site_url('pemesanan/hapus_foto_bayar/')?><?php echo $sql->id_beli;?>" class="bg1 bo-rad-23 hov1 s-text1 trans-0-4 p-l-20 p-r-20 p-t-20 p-b-20 m-r-20">
							<span class="lnr lnr-pencil fs-20 p-r-10"></span> Ubah Foto Bukti
						</a>
					
						<!-- <a href="<?php echo site_url('pemesanan/invoice/');?><?php echo $sql->id_beli;?>" class="bg1 bo-rad-23 hov1 s-text1 trans-0-4 p-l-20 p-r-20 p-t-20 p-b-20">
						<span class="lnr lnr-download fs-20 p-r-10"></span> Download Invoice
						</a> -->
						<a href="#" id="btn-form-id" onclick="document.getElementById('form-inv-id').submit();" class="bg1 bo-rad-23 hov1 s-text1 trans-0-4 p-l-20 p-r-20 p-t-20 p-b-20">
						<span class="lnr lnr-download fs-20 p-r-10"></span> Download Invoice
						</a>
					</div>
					<?php
						}else{
					?>
					<label>Foto Bukti Pembayaran</label>
					<div class="bo4 of-hidden size15 m-b-20">
						<input class="sizefull s-text7 p-l-22 p-r-22 p-t-10" type="file" name="foto_bukti">
					</div>
					<?php
						}
					}?>
				</div>
				<div class="col-md-4">
					<?php 
					foreach($main['sql2']->result() as $sql){
					?>
					<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
					<h5 class="m-text20 p-b-24">
						Keranjang Total
					</h5>

					<div class="flex-w flex-sb-m p-b-12">
						<span class="s-text18 w-size19 w-full-sm">
							Subtotal:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->beli_harga);?>
						</span>
					</div>

					<div class="flex-w flex-sb bo10 p-t-15 p-b-20">
						<span class="s-text18 w-size19 w-full-sm">
							Jumlah:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo $sql->jumlah_total;?>
						</span>

						<span class="s-text18 w-size19 w-full-sm">
							Bonus:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo $sql->bonus;?>
						</span>

						<span class="s-text18 w-size19 w-full-sm">
							Diskon:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->diskon);?>
						</span>
					</div>

					<div class="flex-w flex-sb-m p-t-26 p-b-30">
						<span class="m-text22 w-size19 w-full-sm">
							Total:
						</span>

						<span class="m-text21 w-size20 w-full-sm">
							<?php echo convert_to_rupiah($sql->total_beli);?>
						</span>
					</div>
					<?php
					}
					?>
					<div class="size15 trans-0-4">
						<button type="submit" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Simpan Pembayaran
						</button>
					</div>
				</div>
			</div>
			<?php echo form_close()?>
		</div>
	</div>
</section>

<div class="modal fade" id="modal_pesanan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Pesanan</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		  <h6>Apakah Pesanan sudah termasuk dengan guru yang mengajar ?</h6>
		<input type="hidden" name="id_keranjang" id="id_keranjang"">
		<input type="hidden" name="harga_total" id="harga_total">
		<input type="hidden" name="jumlah" id="jumlah">
	  </div>
	  <div class="modal-footer">
	  <button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Belum</button>
		<button type="button" type="submit" id="btn_pesan" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Sudah
		</button>
	  </div>
	</div>
  </div>
</div>

<form action="<?php echo site_url('Pdf_Invoice/invoice')?>" id="form-inv-id" method="post">
	<input type="hidden" name="id_beli" value="<?php echo $sql->id_beli;?>">
</form>
<script>
	var form = document.getElementById("form-id");

	document.getElementById("btn-form-id").addEventListener("click", function () {
	form.submit();
	});
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.item_pesan').on('click',function(){
		var id_keranjang = $(this).data('id_keranjang');
		var harga_total = $(this).data('harga_total');
		var jumlah = $(this).data('jumlah');
		
		$('#modal_pesanan').modal('show');
		$('[name="id_keranjang"]').val(id_keranjang);
		$('[name="harga_total"]').val(harga_total);
		$('[name="jumlah"]').val(jumlah);
	});

	$('#btn_pesan').on('click',function(){
		var id_keranjang = $('#id_keranjang').val();
		var jumlah = $('#jumlah').val();
		var harga_total = $('#harga_total').val();
		$.ajax({
				type : "POST",
				url  : "<?php echo site_url('keranjang/create_pemesanan')?>",
				dataType : "JSON",
				data : {id_keranjang:id_keranjang, jumlah:jumlah , harga_total:harga_total},
				success: function(data){
					$('[name="id_keranjang"]').val("");
					$('[name="jumlah"]').val("");
					$('[name="harga_total"]').val("");
					$('#modal_pesanan').modal('hide');
					swal("", "Pesanan berhasil dibuat !", "success");
					window.location.href = "<?php echo site_url('pembayaran');?>";
				}
		});
		return false;
	});
});
</script>
