<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<section class="cart bgwhite p-t-70 p-b-100">
	<div class="container">
		<h1 class="text-center">Keranjang Belanja</h1>
		<br><br>
		<!-- Cart item -->
		<div class="container-table-cart pos-relative">
			<div class="wrap-table-shopping-cart bgwhite">
				<table class="table-shopping-cart">
					<thead>
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">Produk</th>
							<th class="column-3">Harga</th>
							<th class="column-4 p-l-70">Kuantitas</th>
							<th class="column-5">Total</th>
							<th class="column-5">Pilihan</th>
						</tr>
					</thead>

					<tbody>
						<?php
							$data = $main['sql']->result();
							$cek = $main['sql']->num_rows();
							if($cek==0){
						?>
						<tr>
							<td>
								<h6 align="center">Keranjang Masih Kosong :(</h6>
							</td>
						</tr>
						<?php
							}else{
								foreach($data as $sql){
						?>
						<?php echo form_open_multipart('keranjang/edit_keranjang/');?>
						<input type="hidden" name="harga_produk" value="<?php echo $sql->harga_produk;?>">
						<input type="hidden" name="id" value="<?php echo $sql->id_keranjang;?>">
						<tr class="table-row">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="http://localhost/jsit_koperasi_admin/upload/produk/<?php echo $sql->foto_produk;?>" alt="IMG-PRODUCT">
								</div>
							</td>
							<td class="column-2"><?php echo $sql->nama_kategori;?> <?php echo $sql->judul_produk;?></td>
							<td class="column-3"><?php echo convert_to_rupiah($sql->harga_produk);?></td>
							<td class="column-4">
								<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>

									<input class="size8 m-text18 t-center num-product" type="number" name="jumlah" value="<?php echo $sql->jumlah;?>">

									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</td>
							<td class="column-5"><?php echo convert_to_rupiah($sql->harga_total);?></td>
							<td class="column-5">
								<button type="submit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">
									Update Keranjang
								</button>
								<br>
							</td>
						</tr>
						</form>
						<?php
								}
							}
						?>
						<tr class="table-row">
						<?php
						if($main['sql']->num_rows()==0){

						}else{
						?>
							<td class="column-1"></td>
							<td class="column-2"></td>
							<td class="column-3">Jumlah : </td>
							<?php 
							foreach($main['sql2']->result() as $obj){
								$jumlah = $obj->jumlah;
								$total = $obj->total;
							}
							?>
							<td class="column-4"><?php echo $jumlah;?></td>
							<td class="column-5">Rp <?php echo convert_to_rupiah($total);?></td>
							<?php 
							foreach($main['sql']->result() as $sql){
							?>
							<input type="hidden" name="id_keranjang[]" value="<?php echo $sql->id_keranjang;?>">
							<input type="hidden" name="jumlah[]" value="<?php echo $sql->jumlah;?>">
							<?php
							}
							?>
							<td class="column-5">
								<input type="hidden" name="jumlah_total" value="<?php echo $jumlah;?>">
								<input type="hidden" name="harga_total" value="<?php echo $total;?>">
								<a href="#" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4 text-white" data-toggle="modal" data-target=".modal_pesanan">
									Pesan
								</a>
								<br>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<div class="modal fade modal_pesanan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Pesanan</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		  <h6>Apakah Pesanan sudah termasuk dengan guru yang mengajar ?</h6>
	  </div>
	  <div class="modal-footer">
	  <button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Belum</button>
		<a href="#" class="flex-c-m size17 bg4 bo-rad-23 hov1 s-text14 trans-0-4 text-white" data-toggle="modal" data-target=".modal_sk">
			Sudah
		</a>
	  </div>
	</div>
  </div>
</div>

<?php 
foreach($main['sql']->result() as $sql){
?>
<?php echo form_open_multipart('keranjang/create_beli/');?>
<input type="hidden" name="id_keranjang[]" value="<?php echo $sql->id_keranjang;?>">
<input type="hidden" name="jumlah[]" value="<?php echo $sql->jumlah;?>">
<input type="hidden" name="jumlah_total" value="<?php echo $jumlah;?>">
<input type="hidden" name="harga_total" value="<?php echo $total;?>">
<div class="modal fade modal_sk" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Pesanan</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<h6>Baca Syarat dan Ketentuan terlebih dahulu untuk melanjutkan proses pemesanan. <a href="<?php echo base_url();?>upload/sk.pdf" target="__BLANK">Klik disini.</a></h6>
		<br>
		<div class="form-check container">
			<input type="checkbox" class="form-check-input" id="cek" onclick="myFunction()">
			<label for="cek">Saya menyetujui <a href="<?php echo base_url();?>upload/sk.pdf" target="__BLANK">Syarat dan Ketentuan</a> pemesanan</label>
		</div>
	  </div>
	  <div class="modal-footer">
	  <button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Kembali</button>
		<button type="submit" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" id="pesan" disabled="true">
			Pesan
		</button>
	  </div>
	</div>
  </div>
</div>
<?php echo form_close();?>
<?php
}
?>

<script>
	function myFunction(){
		var cek = document.getElementById("cek");
		var pesan = document.getElementById("pesan");
		if(cek.checked == true){
			pesan.disabled = false;
		} else {
			pesan.disabled = true;
		}
	}
</script>

<div class="modal fade" id="modal_pesanan_cp" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Pesanan</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<div class="form-group">
			<div class="bo4 of-hidden size15 m-b-20">
				<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="cp" id="cp" placeholder="Contact Person Pemesan" required>
			</div>
		</div>
		<div class="form-group">
			<div class="bo4 of-hidden size15 m-b-20">
				<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="no_handphone" id="no_handphone" placeholder="Nomor Handphone" required>
			</div>
		</div>
		<input type="text" name="id_keranjang" id="id_keranjang"">
		<input type="text" name="harga_total" id="harga_total">
		<input type="text" name="jumlah" id="jumlah">
	  </div>
	  <div class="modal-footer">
	  <button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Tidak</button>
		<button type="button" type="submit" id="btn_pesan_fix" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Iya
		</button>
	  </div>
	</div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.item_pesan').on('click',function(){
		var id_keranjang = $(this).data('id_keranjang');
		var harga_total = $(this).data('harga_total');
		var jumlah = $(this).data('jumlah');
		
		$('#modal_pesanan').modal('show');
		$('[name="id_keranjang"]').val(id_keranjang);
		$('[name="harga_total"]').val(harga_total);
		$('[name="jumlah"]').val(jumlah);
	});

	$('#btn_pesan').on('click',function(){
		$('#modal_pesanan').modal('hide');
		var id_keranjang = $('#id_keranjang').val();
		var jumlah = $('#jumlah').val();
		var harga_total = $('#harga_total').val();

		$('#modal_pesanan_cp').modal('show');
		$('[name="id_keranjang"]').val(id_keranjang);
		$('[name="harga_total"]').val(harga_total);
		$('[name="jumlah"]').val(jumlah);
	});

	$('#btn_pesan_fix').on('click',function(){
		var id_keranjang = $('#id_keranjang').val();
		var jumlah = $('#jumlah').val();
		var harga_total = $('#harga_total').val();
		var cp = $('#cp').val();
		var no_handphone = $('#no_handphone').val();
		$.ajax({
				type : "POST",
				url  : "<?php echo site_url('keranjang/create_pemesanan')?>",
				dataType : "JSON",
				data : {id_keranjang:id_keranjang, jumlah:jumlah , harga_total:harga_total, cp:cp, no_handphone:no_handphone},
				success: function(data){
					$('[name="id_keranjang"]').val("");
					$('[name="jumlah"]').val("");
					$('[name="harga_total"]').val("");
					$('[name="cp"]').val("");
					$('[name="no_handphone"]').val("");
					$('#modal_pesanan_cp').modal('hide');
					window.location.href = "<?php echo site_url('keranjang')?>";
					swal("", "Pesanan berhasil dibuat !", "success");
				}
		});
		return false;
	});
});
</script>
