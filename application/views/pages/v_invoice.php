<div class="col-md-6 m-t-20 m-b-20">
    <div class="invoice p-b-15">
        <div class="row container-fluid">
            <div class="col-md-4">
                <img src="<?php echo base_url('assets/images/koperasi.png');?>" alt="Not found!" width="100%" class="m-t-15">
            </div>
            <div class="col-md-8 m-t-15">
                <p class="text-black">KOPERASI BERKAH USAHA TERPADU <br>
                    JSIT  INDONESIA <br>
                    Pondok Nurul Fikri,Jl Tugu Raya Areman Blok R2 Tugu Cimanggis Depok <br>
                    Telp.021 - 22867102 <br>
                    E - mail : Koperasijsitindonesia@gmail.com
                </p>
            </div>
        </div><br>
        <div class="container-fluid">
            <h3 class="text-center alert-danger text-black py-2">INVOICE</h3><br>
            <div class="row">
                <div class="col-md-4">
                    <p class="text-black">Invoice</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
                <div class="col-md-4">
                    <p class="text-black">Kepada</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
                <div class="col-md-4">
                    <p class="text-black">Kode Pelanggan</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
                <div class="col-md-4">
                    <p class="text-black">Alamat</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
                <div class="col-md-4">
                    <p class="text-black">CP</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
                <div class="col-md-4">
                    <p class="text-black">No Telepon</p>
                </div>
                <div class="col-md-1"><p class="text-black">:</p></div>
                <div class="col-md-7"><p class="text-black">043 / XII / TEMATIK / 2019</p></div>
            </div><br>
            <table class="table table-bordered">
                <thead class="alert-danger">
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                        $data = $main['sql']->result();
                        $cek = $main['sql']->num_rows();
                        foreach($data as $sql){
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                        }
                    ?>
                    <tr>
                        <td colspan="4" class="text-right">Subtotal</td>
                        <td class="text-right">-</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">Discount</td>
                        <td class="text-right">-</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">Total</td>
                        <td class="text-right">-</td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <p class="text-black">Terbilang : </p>
            <br>
            <p class="text-black">* Pembayaran Transfer ke Rekening Bank BNI Syariah
                <br>
                No.Rek 8002828252 An Koperasi Berkah Usaha Terpadu
            </p>
            <p class="text-black">* Bukti transfer harap  di kirim Via WA ke No.0812 800 35249 / Didit</p><br>
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <p class="text-black text-center">Depok, 10 Desember 2019</p><br><br>
                    <p class="text-black text-center">Adi Wibowo</p>
                    <hr class="bg-dark m-t-0 m-b-0">
                    <p class="text-black text-center"> Koperasi Berkah Usaha Terpadu</p>
                </div>
            </div>
        </div>
    </div>
</div>
