<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
	<div class="container">
		<h3 class="m-text26 p-t-15 p-b-16">
			Halaman Profile
		</h3>
		<?php echo form_open_multipart('profile/update_member/');?>
		<?php
		foreach($main['sql']->result() as $sql){
		?>
		<input type="hidden" name="id_member" value="<?php echo $sql->id_member;?>">
		<div class="row">
			<div class="col-md-4">
				<label for="id_registrasi">ID Registrasi</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="number" name="id_registrasi" id="id_registrasi" placeholder="ID Registrasi" value="<?php echo $sql->id_registrasi;?>" readonly>
				</div>
				<label for="nama_sekolah">Nama Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="nama_sekolah" id="nama_sekolah" placeholder="Nama Sekolah" value="<?php echo $sql->nama_sekolah;?>">
				</div>
				<label for="jenjang_sekolah">Jenjang Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jenjang_sekolah" id="jenjang_sekolah" placeholder="Jenjang Sekolah" value="<?php echo $sql->jenjang_sekolah;?>">
				</div>
			</div>
			<div class="col-md-4">
				<label for="email_sekolah">Email Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email_sekolah" id="email_sekolah" placeholder="Email Sekolah" value="<?php echo $sql->email_sekolah;?>">
				</div>
				<label for="kode_regional">Regional</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kode_regional" id="kode_regional" placeholder="Regional" value="<?php echo $sql->kode_regional;?>">
				</div>
				<label for="alamat_sekolah">Alamat Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah" value="<?php echo $sql->alamat_sekolah;?>">
					<!-- <textarea class="sizefull s-text7 p-l-22 p-r-22 p-t-20" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah"></!--> -->
				</div>
			</div>
			<div class="col-md-4">
				<label for="provinsi_sekolah">Provinsi</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="provinsi_sekolah" id="provinsi_sekolah" placeholder="Provinsi" value="<?php echo $sql->provinsi_sekolah;?>">
				</div>
				<label for="kabupaten_kota_sekolah">Kabupaten / Kota</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kabupaten_kota_sekolah" id="kabupaten_kota_sekolah" placeholder="Kabupaten / Kota Sekolah" value="<?php echo $sql->kabupaten_kota_sekolah;?>">
				</div>
				<button type="submit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">
					Ubah
				</button>
			</div>
		</div>
		<?php
		}
		?>
		<?php echo form_close();?>
	</div>
</section>
