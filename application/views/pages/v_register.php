<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
	<div class="container">
		<h3 class="m-text26 p-t-15 p-b-16">
			Halaman Register
		</h3>
		<?php echo form_open_multipart('register/create_member/');?>
		<div class="row">
			<div class="col-md-4">
				<label for="id_registrasi">ID Registrasi</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="number" name="id_registrasi" id="id_registrasi" placeholder="ID Registrasi" required>
				</div>
				<label for="nama_sekolah">Nama Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="nama_sekolah" id="nama_sekolah" placeholder="Nama Sekolah" required>
				</div>
				<label for="jenjang_sekolah">Jenjang Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="jenjang_sekolah" id="jenjang_sekolah" placeholder="Jenjang Sekolah" required>
				</div>
			</div>
			<div class="col-md-4">
				<label for="email_sekolah">Email Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email_sekolah" id="email_sekolah" placeholder="Email Sekolah" required>
				</div>
				<label for="kode_regional">Regional</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="kode_regional" id="kode_regional" placeholder="Regional" required>
				</div>
				<label for="alamat_sekolah">Alamat Sekolah</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah" required>
					<!-- <textarea class="sizefull s-text7 p-l-22 p-r-22 p-t-20" name="alamat_sekolah" id="alamat_sekolah" placeholder="Alamat Sekolah"></!-->
				</div>
			</div>
			<div class="col-md-4">
				<label for="provinsi_sekolah">Provinsi</label>
				<div class="bo4 of-hidden size15 m-b-20 p-l-22 p-r-22 p-t-15">
					<select name="provinsi_sekolah" class="provinsi" id="provinsi_sekolah" required></select>
				</div>
				<label for="kabupaten_kota_sekolah">Kabupaten / Kota</label>
				<div class="bo4 of-hidden size15 m-b-20 p-l-22 p-r-22 p-t-15">
					<select name="kabupaten_kota_sekolah" class="kabupaten" id="kabupaten_kota_sekolah" required></select>
				</div>
				<label for="password">Password</label>
				<div class="bo4 of-hidden size15 m-b-20">
					<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" id="password" placeholder="Password" required>
				</div>
				<button type="submit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4">
					Daftar
				</button>
			</div>
		</div>
		<?php echo form_close();?>
	</div>
</section>
