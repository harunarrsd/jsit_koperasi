<!-- Header -->
<header class="header1">
  <!-- Header desktop -->
  <div class="container-menu-header">
    <div class="topbar"></div>
    <div class="wrap_header">
      <a href="<?php echo base_url('.');?>" class="logo">
        <img src="<?php echo base_url();?>assets/images/logo-jsit.png" alt="IMG-LOGO">
      </a>
      <div class="wrap_menu">
        <nav class="menu">
          <ul class="main_menu">
            <li>
              <a href="<?php echo site_url('.');?>">Beranda</a>
            </li>

            <li>
              <a href="<?php echo site_url('produk');?>">Produk</a>
            </li>

            <li>
              <a href="<?php echo site_url('tentang');?>">Tentang</a>
            </li>

            <li>
              <a href="<?php echo site_url('kontak');?>">Kontak</a>
            </li>
          </ul>
        </nav>
      </div>
      <div class="header-icons">
				<?php
					if ($this->session->userdata('udhmasuk')==false) {
				?>
					<a href="<?php echo site_url('login');?>" class="header-wrapicon1 dis-block">
						<!-- <i class="far fa-2x fa-user-circle"></i> -->
						<span class="lnr lnr-user fs-25"></span>
					</a>
				<?php
					} else{
				?>
				<!-- login -->
					<div class="header-wrapicon2">
						<a class="header-icon1 js-show-header-dropdown">
							<!-- <i class="far fa-2x fa-user-circle"></i> -->
							<span class="lnr lnr-user fs-25"></span>
						</a>
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem text-center">
								<li class="header-cart-item">
									<a href="<?php echo site_url('pemesanan');?>" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Pesanan
									</a>
								</li>
							</ul>
							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<a href="<?php echo site_url('profile');?>" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Profile
									</a>
								</div>
								<div class="header-cart-wrapbtn">
									<a href="<?php echo site_url('login/logout');?>" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Logout
									</a>
								</div>
							</div>
						</div>
					</div>
					<span class="linedivide1"></span>
					<div class="header-wrapicon2">
						<a class="header-icon1 js-show-header-notifikasi">
							<!-- <i class="far fa-2x fa-bell"></i> -->
							<span class="lnr lnr-alarm fs-27"></span>
							<div id="show_notifikasi_total">

							</div>
						</a>
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<h6 class="p-b-10">NOTIFIKASI</h6>
								<div id="show_notifikasi">
									
								</div>
								<div id="show_notifikasi_baca">
									
								</div>
							</ul>
						</div>
					</div>
					<span class="linedivide1"></span>
					<div class="header-wrapicon2">
						<a class="header-icon1 js-show-header-dropdown">
							<!-- <i class="fas fa-2x fa-shopping-cart"></i> -->
							<span class="lnr lnr-cart fs-27"></span>
							<div id="show_total_keranjang">

							</div>
						</a>
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<h6>KERANJANG BELANJA</h6>
								<div id="show_keranjang">
									
								</div>
							</ul>
							<div id="show_keranjang_total">

							</div>
							<br>
							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="<?php echo site_url();?>keranjang" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Keranjang
									</a>
								</div>
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="#" id="modal_kosong_keranjang" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Kosongkan
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php
					}
				?>
      </div>
    </div>
  </div>
  <!-- Header Mobile -->
  <div class="wrap_header_mobile">
    <!-- Logo moblie -->
    <a href="index.html" class="logo-mobile">
      <img src="<?php echo base_url();?>assets/images/logo-jsit.png" alt="IMG-LOGO">
    </a>
    <!-- Button show menu -->
    <div class="btn-show-menu">
      <!-- Header Icon mobile -->
      <div class="header-icons-mobile">
        <a href="#" class="header-wrapicon1 dis-block">
          <i class="far fa-2x fa-user-circle"></i>
        </a>
        <span class="linedivide2"></span>
        <div class="header-wrapicon2">
          <a class="header-icon1 js-show-header-dropdown">
            <i class="far fa-2x fa-bell"></i>
            <span class="header-icons-noti">0</span>
          </a>
          <div class="header-cart header-dropdown">
            <ul class="header-cart-wrapitem">
              <h6>NOTIFIKASI</h6>
              <li class="header-cart-item">
                <!-- <div class="header-cart-item-img">
                  <img src="<?php echo base_url();?>assets/images/item-cart-02.jpg" alt="IMG">
                </div>

                <div class="header-cart-item-txt">
                  <a href="#" class="header-cart-item-name">
                    Converse All Star Hi Black Canvas
                  </a>

                  <span class="header-cart-item-info">
                    1 x $39.00
                  </span>
                </div> -->
                <p>Belum ada notifikasi</p>
              </li>
            </ul>
          </div>
        </div>
        <span class="linedivide2"></span>
        <div class="header-wrapicon2">
          <a class="header-icon1 js-show-header-dropdown">
            <i class="fas fa-2x fa-shopping-cart"></i>
            <span class="header-icons-noti">0</span>
          </a>
          <!-- Header cart noti -->
          <div class="header-cart header-dropdown">
            <ul class="header-cart-wrapitem">
              <h6>KERANJANG BELANJA</h6>
              <li class="header-cart-item">
                <!-- <div class="header-cart-item-img">
                  <img src="<?php echo base_url();?>assets/images/item-cart-02.jpg" alt="IMG">
                </div>

                <div class="header-cart-item-txt">
                  <a href="#" class="header-cart-item-name">
                    Converse All Star Hi Black Canvas
                  </a>

                  <span class="header-cart-item-info">
                    1 x $39.00
                  </span>
                </div> -->
              </li>
            </ul>
            <div class="header-cart-total">
              Total: Rp.0
            </div>
            <div class="header-cart-buttons">
              <div class="header-cart-wrapbtn">
                <!-- Button -->
                <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                  Keranjang
                </a>
              </div>
              <div class="header-cart-wrapbtn">
                <!-- Button -->
                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                  Kosongkan
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </div>
    </div>
  </div>

  <!-- Menu Mobile -->
  <div class="wrap-side-menu" >
    <nav class="side-menu">
      <ul class="main-menu">
        <li class="item-menu-mobile">
          <a href="<?php echo site_url('.');?>">Beranda</a>
        </li>

        <li class="item-menu-mobile">
          <a href="<?php echo site_url('produk');?>">Produk</a>
        </li>

        <li class="item-menu-mobile">
          <a href="<?php echo site_url('tentang');?>">Tentang</a>
        </li>

        <li class="item-menu-mobile">
          <a href="<?php echo site_url('kontak');?>">Kontak</a>
        </li>
      </ul>
    </nav>
  </div>
</header>
