<?php
	if ($this->session->userdata('udhmasuk')==false) {
		$id_beli=0;
	}else{
		$notif = $this->db->query("SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = " . $this->session->userdata('id') . " AND  notifikasi.baca = 0 GROUP BY id_notifikasi");
		if($notif->num_rows() == 0){
			$id_beli=0;
		}else{
			foreach($notif->result() as $sql){
				$id_notifikasi = $sql->id_notifikasi;
				$id_beli = $sql->id_beli;
			}
		}
	}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/animsition/js/animsition.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/select2/select2.min.js"></script>
<script type="text/javascript">
	$(".selection-1").select2({
		minimumResultsForSearch: 20,
		dropdownParent: $('#dropDownSelect1')
	});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slick-custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/countdowntime/countdowntime.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/lightbox2/js/lightbox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/all.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		show_product();
		show_keranjang();
		show_notifikasi();
		show_notifikasi_baca();
		show_keranjang_total();

		$('.block2-btn-addcart2').each(function() {
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function() {
				swal(nameProduct, "Berhasil ditambahkan ke keranjang !", "success");
			});
		});

		function convertToRupiah(angka)
        {
        	var rupiah = '';		
        	var angkarev = angka.toString().split('').reverse().join('');
        	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
        }

		function show_product() {
			$.ajax({
				url: '<?php echo site_url('produk/data_produk') ?>',
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						var id_kelas = data[i].id_kelas;
						html +=
							'<div class="col-sm-12 col-md-6 col-lg-3 p-b-50">' +
							'<div class="block2">' +
							'<div class="block2-img wrap-pic-w of-hidden pos-relative">' +
							'<img src="http://localhost/jsit_koperasi_admin/upload/produk/' + data[i].foto_produk + '" alt="IMG-PRODUCT">' +
							'<div class="block2-overlay trans-0-4">' +
							'<div class="block2-btn-addcart w-size1 trans-0-4">' +
							'<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text14 trans-0-4 <?php if ($this->session->userdata('udhmasuk') == true) { echo "item_edit"; } else { echo "login_dulu"; } ?>" data-id_produk="' + data[i].id_produk + '" data-harga_produk="' + data[i].harga_produk + '" data-judul_produk="' + data[i].judul_produk + '" data-stok="' + data[i].stok + '">' +
							'Tambah ke Keranjang' +
							'</button>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="block2-txt p-t-20">' +
							'<p>' +
							'' + data[i].nama_kelas + '' +
							'</p>' +
							'<a href="<?php echo base_url('.'); ?>" class="block2-name dis-block s-text3 p-b-5">' +
							'' + data[i].nama_kategori + ' ' + data[i].judul_produk + '' +
							'</a>' +
							'<span class="block2-price m-text6 p-r-5">' +
							convertToRupiah(data[i].harga_produk) + '' +
							'</span>' +
							'</div>' +
							'</div>' +
							'</div>';
					}
					$('#show_data').html(html);
				}

			});
		}

		function show_keranjang() {
			$.ajax({
				url: '<?php echo site_url('produk/data_keranjang') ?>',
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var html2 = '';
					var i;
					if (data.length == 0) {
						html += '<p>Keranjang masih kosong :(</p>';
					} else {
						for (i = 0; i < data.length; i++) {
							html +=
								'<li class="header-cart-item">' +
								'<div class="header-cart-item-img">' +
								'<img src="http://localhost/jsit_koperasi_admin/upload/produk/' + data[i].foto_produk + '" alt="IMG">' +
								'</div>' +
								'<div class="header-cart-item-txt">' +
								'<a class="header-cart-item-name">' +
								'' + data[i].nama_kategori + ' ' + data[i].judul_produk + '' +
								'</a>' +
								'<span class="header-cart-item-info">' +
								'' + data[i].jumlah + ' <br>' +
								'' + convertToRupiah(data[i].harga_produk) + '' +
								'</span>' +
								'</div>' +
								'</li>';
							html2 += '<span class="header-icons-noti">' + data.length + '</span>';
						}
					}
					$('#show_keranjang').html(html);
					$('#show_total_keranjang').html(html2);
				}

			});
		}

		function show_notifikasi() {
			$.ajax({
				url: '<?php echo site_url('main/data_notifikasi') ?>',
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var html2 = '';
					var i;
					// if (data.length == 0) {
					// 	html += '<p>Belum ada notifikasi</p>';
					// } else {
						for (i = 0; i < data.length; i++) {
							html +=
								'<li class="header-cart-item badge-dark p-l-10 p-r-10">' +
								'<div>' +
								'<a class="header-cart-item-name">' +
								'' + data[i].pesan + '' +
								'</a>' +
								'</div>' +
								'</li>';
							html2 += '<span class="header-icons-noti">' + data.length + '</span>';
						}
					// }
					$('#show_notifikasi').html(html);
					$('#show_notifikasi_total').html(html2);
				}
			});
		}

		function show_notifikasi_baca() {
			$.ajax({
				url: '<?php echo site_url('main/data_notifikasi_baca') ?>',
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var html2 = '';
					var i;
					if (data.length == 0) {
						html += '<p>Belum ada notifikasi</p>';
					} else {
						for (i = 0; i < data.length; i++) {
							html +=
								'<li class="header-cart-item p-l-10 p-r-10">' +
								'<div>' +
								'<a class="header-cart-item-name">' +
								'' + data[i].pesan + '' +
								'</a>' +
								'</div>' +
								'</li>';
						}
					}
					$('#show_notifikasi_baca').html(html);
				}
			});
		}

		function update_notifikasi(){
			var id_beli = "<?php echo $id_beli;?>";
			if(id_beli==0){
				
			}else{
				$.ajax({
					type : "POST",
					url  : "<?php echo site_url('main/update_notif');?>",
					dataType : "JSON",
					data : {id_beli:id_beli},
					success: function(data){
						show_product();
						show_keranjang();
						show_notifikasi();
						show_notifikasi_baca();
						show_keranjang_total();
					},
					error: function(){
						swal("", "Gagal !", "error");
					}
				});
				return false;
			}
		}

		function show_keranjang_total() {
			$.ajax({
				url: '<?php echo site_url('produk/data_keranjang_total') ?>',
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						if (data[i].total == null) {
							html +=
								'<div class="header-cart-total">' +
								'Total: 0' +
								'</div>';
						} else {
							html +=
								'<div class="header-cart-total">' +
								'Total: ' + convertToRupiah(data[i].total) + '' +
								'</div>';
						}
					}
					$('#show_keranjang_total').html(html);
				}

			});
		}

		$('#btn_save').on('click', function() {
			var id_produk = $('#id_produk').val();
			var jumlah = $('#jumlah').val();
			var harga_produk = $('#harga_produk').val();
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('produk/create') ?>",
				dataType: "JSON",
				data: {
					id_produk: id_produk,
					jumlah: jumlah,
					harga_produk: harga_produk
				},
				success: function(data) {
					$('[name="id_produk"]').val("");
					$('[name="jumlah"]').val("");
					$('[name="harga_produk"]').val("");
					$('#keranjangg').modal('hide');
					$('#keranjangg_main').modal('hide');
					swal("", "Berhasil ditambahkan ke keranjang !", "success");
					show_product();
					show_keranjang();
					show_keranjang_total();
				}
			});
			return false;
		});

		$('#show_data').on('click', '.item_edit', function() {
			var judul_produk = $(this).data('judul_produk');
			var harga_produk = $(this).data('harga_produk');
			var id_produk = $(this).data('id_produk');
			var stok = $(this).data('stok');

			$('#keranjangg').modal('show');
			$('[name="id_produk"]').val(id_produk);
			$('[name="harga_produk"]').val(harga_produk);
			$('[name="stok"]').val(stok);
		});

		$('.item_edit_main').on('click', function() {
			var judul_produk = $(this).data('judul_produk');
			var harga_produk = $(this).data('harga_produk');
			var id_produk = $(this).data('id_produk');
			var stok = $(this).data('stok');

			$('#keranjangg_main').modal('show');
			$('[name="id_produk"]').val(id_produk);
			$('[name="harga_produk"]').val(harga_produk);
			$('[name="stok"]').val(stok);
		});

		$('.login_dulu_main').on('click', function() {
			$('#login_dulu_main').modal('show');
		});

		$('#show_data').on('click', '.login_dulu', function() {
			$('#login_dulu').modal('show');
		});

		$('#modal_kosong_keranjang').on('click', function() {
			$('.kosong_keranjang').modal('show');
		});

		$('#btn_kosong_keranjang').on('click', function() {
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('produk/kosong_keranjang') ?>",
				dataType: "JSON",
				success: function(data) {
					$('.kosong_keranjang').modal('hide');
					swal("", "Keranjang berhasil dikosongkan !", "success");
					show_product();
					show_keranjang();
					// show_keranjang_total();
				}
			});
			return false;
		});

		$('#id_registrasi').on('input',function(){
                
			var id_registrasi=$(this).val();
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url('register/get_member')?>",
				dataType : "JSON",
				data : {id_registrasi: id_registrasi},
				cache:false,
				success: function(data){
					$.each(data,function(id_registrasi, nama_sekolah, jenjang_sekolah, email_sekolah, nama_regional, alamat, provinsi, kabupaten_kotamadya){
						$('[name="nama_sekolah"]').val(data.nama_sekolah);
						$('[name="jenjang_sekolah"]').val(data.jenjang_sekolah);
						$('[name="email_sekolah"]').val(data.email_sekolah);
						$('[name="kode_regional"]').val(data.nama_regional);
						$('[name="alamat_sekolah"]').val(data.alamat);
						html = '<option>'+data.provinsi+'</option>';
						$('.provinsi').html(html);
						html2 = '<option>'+data.kabupaten_kotamadya+'</option>';
						$('.kabupaten').html(html2);
						// $('[name="provinsi_sekolah"]').val(data.provinsi);
						// $('[name="kabupaten_kota_sekolah"]').val(data.kabupaten_kotamadya);
					});
				},
				error: function(){
					$('[name="nama_sekolah"]').val('');
					$('[name="jenjang_sekolah"]').val('');
					$('[name="email_sekolah"]').val('');
					$('[name="kode_regional"]').val('');
					$('[name="alamat_sekolah"]').val('');
					html = '<option></option>';
					$('.provinsi').html(html);
					html2 = '<option></option>';
					$('.kabupaten').html(html2);
					// $('[name="provinsi_sekolah"]').val('');
					// $('[name="kabupaten_kota_sekolah"]').val('');
				}
			});
			return false;
		});
		$('.js-show-header-notifikasi').on('click', function(){
			$(this).parent().find('.header-dropdown');
		});

		var menu = $('.js-show-header-notifikasi');
		var sub_menu_is_showed = -1;

		for(var i=0; i<menu.length; i++){
			$(menu[i]).on('click', function(){ 
				
					if(jQuery.inArray( this, menu ) == sub_menu_is_showed){
						$(this).parent().find('.header-dropdown').toggleClass('show-header-dropdown');
						sub_menu_is_showed = -1;
						update_notifikasi();
					}
					else {
						for (var i = 0; i < menu.length; i++) {
							$(menu[i]).parent().find('.header-dropdown').removeClass("show-header-dropdown");
						}

						$(this).parent().find('.header-dropdown').toggleClass('show-header-dropdown');
						sub_menu_is_showed = jQuery.inArray( this, menu );
					}
			});
		}

		$(".js-show-header-notifikasi, .header-dropdown").click(function(event){
			event.stopPropagation();
		});

		$(window).on("click", function(){
			for (var i = 0; i < menu.length; i++) {
				$(menu[i]).parent().find('.header-dropdown').removeClass("show-header-dropdown");
			}
			sub_menu_is_showed = -1;
		});

		$('.provinsi').select2({
			placeholder: 'Provinsi',
			ajax: {
				url: '<?php echo base_url();?>register/provinsi',
				dataType: 'json',
				delay: 250,
				data: function(params){
					return{
						provinsi : params.term
					};
				},
				processResults: function (data) {
					var results = [];

					$.each(data, function(index, item){
						results.push({
							id: item.id_provinsi,
							text: item.nama_provinsi
						});
					});
					return{
						results: results
					};
				}
			}
		});

		$('.kabupaten').select2({
			placeholder: 'Kabupaten / Kota',
			ajax: {
				url: '<?php echo base_url();?>register/kabkot',
				dataType: 'json',
				delay: 250,
				data: function(params){
					return{
						kabkot : params.term
					};
				},
				processResults: function (data) {
					var results = [];

					$.each(data, function(index, item){
						results.push({
							id: item.id_kk,
							text: item.nama
						});
					});
					return{
						results: results
					};
				}
			}
		});
	});
</script>
