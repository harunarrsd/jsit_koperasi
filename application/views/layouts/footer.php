<!-- Footer -->
<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
	<div class="row container">
		<div class="col-sm-12 col-lg-6">
			<h4 class="s-text12 p-b-30">
				Hubungi Kami
			</h4>
			<div>
				<p class="s-text7 w-size27">
					Punya pertanyaan atau saran? Beri tahu kami di Jl. Tugu Raya, Tugu, Kec. Cimanggis, Kota Depok, Jawa Barat 16451 atau melalui Contact Person : Anis (0813-8110-88770), Nadia (0857-7192-8944)
				</p>
			</div>
		</div>
		<div class="col-sm-12 col-lg-3">
			<h4 class="s-text12 p-b-30">
				Halaman
			</h4>
			<ul>
				<li class="p-b-9">
					<a href="<?php echo base_url('.');?>" class="s-text7">
						Beranda
					</a>
				</li>
				<li class="p-b-9">
					<a href="<?php echo base_url('produk');?>" class="s-text7">
						Produk
					</a>
				</li>
				<li class="p-b-9">
					<a href="<?php echo base_url('tentang');?>" class="s-text7">
						Tentang
					</a>
				</li>
				<li class="p-b-9">
					<a href="<?php echo base_url('kontak');?>" class="s-text7">
						Kontak
					</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-12 col-lg-3">
			<h4 class="s-text12 p-b-30">
				Media Sosial
			</h4>
			<div>
				<div class="flex-m">
					<a href="<?php echo base_url('.');?>" class="fs-30 color1 p-r-20"><i class="fab fa-facebook-square"></i></a>
					<a href="<?php echo base_url('.');?>" class="fs-30 color1 p-r-20"><i class="fab fa-instagram"></i></a>
					<a href="<?php echo base_url('.');?>" class="fs-30 color1 p-r-20"><i class="fab fa-whatsapp"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="t-center p-l-15 p-r-15">
		<div class="t-center s-text8 p-t-20">
			Copyright © 2019 All rights reserved. | <a href="#" target="_blank">Koperasi JSIT</a>
		</div>
	</div>
</footer>
<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
	<span class="symbol-btn-back-to-top">
		<i class="fa fa-angle-double-up" aria-hidden="true"></i>
	</span>
</div>
<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>