<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layouts/head') ?>
</head>
<body>

<!-- <div class="wrapper"> -->

    <!-- header -->
    <?php $this->load->view('layouts/header')?>
    <!-- END header -->

    <!-- <div class="content-wrapper"> -->
        <!-- pages -->
        <?php echo $main['pages']; ?>
        <!-- END pages -->
	<!-- </div> -->
	
<div class="modal fade kosong_keranjang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
		  <h6>Information</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
		<p>Apakah Anda yakin ingin mengkonsongkan keranjang ?</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4" data-dismiss="modal">Tidak</button>
		<button type="button" type="submit" id="btn_kosong_keranjang" class="size17 bg1 bo-rad-23 hov1 s-text14 trans-0-4">
			Iya
		</button>
	  </div>
	</div>
  </div>
</div>
    
    <!-- footer -->
    <?php $this->load->view('layouts/footer')?>
    <!-- END footer -->
<!-- </div> -->

    <!-- javascript -->
    <?php $this->load->view('layouts/javascript')?>
    <!-- END javascript -->
</body>
</html>
