<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layouts/head') ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/invoice.css">
</head>
<body>
    <!-- <div class="content-wrapper"> -->
        <!-- pages -->
        <?php echo $main['pages']; ?>
        <!-- END pages -->
	<!-- </div> -->

    <!-- javascript -->
    <?php $this->load->view('layouts/javascript')?>
    <!-- END javascript -->
</body>
</html>
