<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_koperasi extends CI_Model
{

	// login
	function proseslogin($user, $pass)
	{
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		return $this->db->get('user')->row();
	}

	function read_beranda()
	{
		return $this->db->query("SELECT * FROM config_beranda");
	}

	function read_member()
	{
		return $this->db->query("SELECT * FROM member JOIN user using(id_member) WHERE user.id_user = ".$this->session->userdata('id')."");
	}

	// produk
	function read_produk()
	{
		return $this->db->query("SELECT * FROM produk JOIN kelas using(id_kelas) JOIN kategori using(id_kategori)");
	}

	// pemesanan
	function create_detail_beli($data){
        return $this->db->insert_batch('detail_beli',$data);
	}
	function create_beli()
	{
		$jumlah_total = $this->input->post('jumlah_total');
		$harga_total = $this->input->post('harga_total');
		$sql = $this->db->query('SELECT * FROM member WHERE id_member = '.$this->session->userdata('id_member').'')->result();
		foreach($sql as $query){
			$kabkot = $query->kabupaten_kota_sekolah;
		}
		$sql2 = $this->db->query('SELECT * FROM daerah_tertinggal')->result();
		foreach($sql2 as $query2){
			$kabkot2 = $query2->nama;
		}
		if($kabkot==$kabkot2){
			$diskon = 0.35*$harga_total;
		}else if($jumlah_total>=4000){
			$diskon = 0.4*$harga_total;
		}else if($jumlah_total>=3700){
			$diskon = 0.38*$harga_total;
		}else if($jumlah_total<3700){
			$diskon = 0.35*$harga_total;
		}else if($jumlah_total<200){
			$diskon = 0;
		}
		if($jumlah_total>=50){
			$bonus = 1;
		}else{
			$bonus = 0;
		}
		$data = array(
			'jumlah_total'  => $jumlah_total,
			'bonus'  => $bonus,
			'harga_total' => $harga_total,
			'diskon' => $diskon,
			'total_beli' => $harga_total - $diskon,
			'tanggal_beli' => date("Y-m-d H:i:s"),
			'status' => 'Baru'
		);
		$result = $this->db->insert('beli', $data);
		return $result;
	}
	function create_pemesanan()
	{
		// if($this->input->post('jumlah') >= 0 AND $this->input->post('jumlah') < 200){
		// 	$diskon = 0;
		// 	$total_beli = $this->input->post('harga_total');
		// } else if($this->input->post('jumlah') > 200 AND $this->input->post('jumlah') < 3700){
		// 	$harga_total = $this->input->post('harga_total');
		// 	$diskon = 0.35 * $harga_total;
		// 	$total_beli = $harga_total - $diskon;
		// } else if($this->input->post('jumlah') >= 3700 AND $this->input->post('jumlah') < 4000){
		// 	$harga_total = $this->input->post('harga_total');
		// 	$diskon = 0.38 * $harga_total;
		// 	$total_beli = $harga_total - $diskon;
		// } else if($this->input->post('jumlah') >= 4000){
		// 	$harga_total = $this->input->post('harga_total');
		// 	$diskon = 0.4 * $harga_total;
		// 	$total_beli = $harga_total - $diskon;
		// }
		$data = array(
			'id_keranjang'  => $this->input->post('id_keranjang'),
			'diskon'  => 0,
			'total_beli' => $this->input->post('harga_total'),
			'tanggal_beli' => date("Y-m-d H:i:s"),
			'status' => 'Baru',
			'foto_bukti_pembayaran' => null,
			'cp' => $this->input->post('cp'),
			'no_handphone' => $this->input->post('no_handphone')
		);
		$result = $this->db->insert('beli', $data);
		return $result;
	}
	function read_pemesanan()
	{
		return $this->db->query("SELECT * FROM beli JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang) where keranjang.id_user = " . $this->session->userdata('id') . " AND keranjang.beli = 1 AND beli.status != 'Batal' GROUP BY id_beli");
	}

	function read_pembayaran($id_beli)
	{
		// $id_beli = $this->input->post('id_beli');
		// $id_beli = 30;
		return $this->db->query("SELECT *,beli.harga_total as beli_harga FROM pembayaran JOIN beli using(id_beli) JOIN detail_beli using(id_beli) join keranjang using(id_keranjang) where keranjang.id_user = " . $this->session->userdata('id') . " AND keranjang.beli = 1 AND pembayaran.id_beli = ".$id_beli." GROUP BY id_beli");
	}

	function read_detail_beli()
	{
		return $this->db->query("SELECT * FROM detail_beli JOIN beli using(id_beli) join keranjang using(id_keranjang) JOIN produk using(id_produk) where keranjang.id_user = " . $this->session->userdata('id') . " AND keranjang.beli = 1");
	}

	function read_beli()
	{
		return $this->db->query("SELECT *, beli.harga_total as beli_harga FROM beli JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang) where keranjang.id_user = " . $this->session->userdata('id') . " AND keranjang.beli = 1 GROUP BY id_beli");
	}

	// function update_pemesanan(){
	// 	$id_keranjang=$this->input->post('id_keranjang');
	// 	$cp=$this->input->post('cp');
	// 	$no_handphone=$this->input->post('no_handphone');

	// 	$this->db->set('cp', $cp);
	// 	$this->db->set('no_handphone', $no_handphone);
	// 	$this->db->where('id_keranjang', $id_keranjang);
	// 	$result=$this->db->update('beli');
	// 	return $result;
	// }


	function read_tentang()
	{
		return $this->db->query("SELECT * FROM config_tentang");
	}
	function read_kontak()
	{
		return $this->db->query("SELECT * FROM config_kontak");
	}
	function read_kelas()
	{
		return $this->db->query("SELECT * FROM kelas");
	}
	function read_kategori()
	{
		return $this->db->query("SELECT * FROM kategori");
	}
	function read_etalase_produk()
	{
		return $this->db->query("SELECT * FROM produk JOIN kelas using(id_kelas) join kategori using(id_kategori) ORDER by produk.updated_date DESC LIMIT 5");
	}

	// keranjang
	function read_keranjang()
	{
		return $this->db->query("SELECT * FROM keranjang JOIN produk using(id_produk) join kategori using(id_kategori) JOIN kelas using(id_kelas) where keranjang.id_user = " . $this->session->userdata('id') . " AND keranjang.beli = 0");
	}
	function read_keranjang_total()
	{
		return $this->db->query("SELECT SUM(harga_total) as total, SUM(jumlah) as jumlah FROM keranjang where id_user = " . $this->session->userdata('id') . " AND beli = 0");
	}

	function kosong_keranjang()
	{
		$id_user = $this->session->userdata('id');
		$this->db->where('id_user', $id_user);
		$result = $this->db->delete('keranjang');
		return $result;
	}

	function create_keranjang()
	{
		$data = array(
			'id_produk'  => $this->input->post('id_produk'),
			'jumlah'  => $this->input->post('jumlah'),
			'harga_produk' => $this->input->post('harga_produk'),
			'harga_total' => $this->input->post('jumlah') * $this->input->post('harga_produk'),
			'id_user' => $this->session->userdata('id'),
			'created_date' => date("Y-m-d H:i:s"),
			'beli' => 0
		);
		$result = $this->db->insert('keranjang', $data);
		return $result;
	}

	function update_keranjang($id, $data)
	{
		$this->db->where("id_keranjang", $id);
		$this->db->update('keranjang', $data);
	}

	function update_beli($id, $data)
	{
		$this->db->where("id_beli", $id);
		$this->db->update('beli', $data);
	}

	function update_pembayaran($id, $data)
	{
		$this->db->where("id_beli", $id);
		$this->db->update('pembayaran', $data);
	}

	function update_member($id, $data)
	{
		$this->db->where("id_member", $id);
		$this->db->update('member', $data);
	}

	function update_notif(){
		$id_beli=$this->input->post('id_beli');
		$baca=1;

		$this->db->set('baca', $baca);
		$this->db->where('id_beli', $id_beli);
		$result=$this->db->update('notifikasi');
		return $result;
	}

	// notifikasi
	function read_notifikasi()
	{
		return $this->db->query("SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = " . $this->session->userdata('id') . " AND  notifikasi.baca = 0 GROUP BY id_notifikasi");
	}
	function read_notifikasi_baca()
	{
		return $this->db->query("SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = " . $this->session->userdata('id') . " AND  notifikasi.baca = 1 GROUP BY id_notifikasi DESC");
	}
	function read_pembatalan_pesanan()
	{
		return $this->db->query("SELECT * FROM notifikasi JOIN detail_beli using(id_beli) JOIN keranjang using(id_keranjang)  where keranjang.id_user = " . $this->session->userdata('id') . " AND tgl_pembatalan IS NOT NULL GROUP BY id_notifikasi");
	}
	function read_notifikasi_total()
	{
		return $this->db->query("SELECT * FROM notifikasi join detail_beli using(id_beli) join keranjang using(id_keranjang) where keranjang.id_user = " . $this->session->userdata('id') . " AND  notifikasi.baca = 0 GROUP BY keranjang.id_keranjang");
	}

	function detail_pesanan($id){
		$this->db->where("id_beli",$id);
		$this->db->join('keranjang','id_keranjang');
		$this->db->join('produk','id_produk');
		$this->db->join('kelas','id_kelas');
		$this->db->join('kategori','id_kategori');
		return $this->db->get('detail_beli');
	}
	
	function delete_foto_bayar($id){
        $this->db->where("id_beli", $id);
        $query = $this->db->get('pembayaran');
        $row = $query->row();
        unlink("upload/bukti_bayar/$row->foto_bukti_pembayaran");
	}
	
	function create_member()
	{
		$data = array(
			'id_registrasi'  => $this->input->post('id_registrasi'),
			'nama_sekolah'  => $this->input->post('nama_sekolah'),
			'jenjang_sekolah' => $this->input->post('jenjang_sekolah'),
			'alamat_sekolah' => $this->input->post('alamat_sekolah'),
			'id_js' => $this->input->post('id_js'),
			'email_sekolah' => $this->input->post('email_sekolah'),
			'provinsi_sekolah' => $this->input->post('provinsi_sekolah'),
			'id_provinsi' => $this->input->post('id_provinsi'),
			'kabupaten_kota_sekolah' => $this->input->post('kabupaten_kota_sekolah'),
			'id_kabkot' => $this->input->post('id_kabkot'),
			'kordinat_geografis' => $this->input->post('id_kabkot'),
			'kode_regional' => $this->input->post('kode_regional'),
		);
		$result = $this->db->insert('member', $data);
		return $result;
	}
	function create_user($data)
	{
		$result = $this->db->insert('user', $data);
		return $result;
	}
	function create_komplain($data)
	{
		$result = $this->db->insert('komplain', $data);
		return $result;
	}

	function get_member($id_registrasi){
		$hsl=$this->db->query("SELECT * FROM tbl_master_sekolah WHERE id_registrasi='$id_registrasi'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_registrasi' => $data->id_registrasi,
					'nama_sekolah' => $data->nama_sekolah,
					'jenjang_sekolah' => $data->jenjang_sekolah,
					'email_sekolah' => $data->email_sekolah,
					'nama_regional' => $data->nama_regional,
					'alamat' => $data->alamat,
					'provinsi' => $data->provinsi,
					'kabupaten_kotamadya' => $data->kabupaten_kotamadya,
					);
			}
		}
		return $hasil;
	}

	function get_provinsi($provinsi, $column){
		$this->db->select('*');
		$this->db->limit(10);
		$this->db->from('provinsi');
		$this->db->like('nama_provinsi', $provinsi);
		return $this->db->get()->result_array();
	}

	function get_kabkot($kabkot, $column){
		$this->db->select('*');
		$this->db->limit(10);
		$this->db->from('kabupaten_kota');
		$this->db->like('nama', $kabkot);
		return $this->db->get()->result_array();
	}
}
